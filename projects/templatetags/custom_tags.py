from django import template

register = template.Library()


@register.filter
def booleanWord(arg):
    if arg is True:
        return "Yes"
    else:
        return "No"
