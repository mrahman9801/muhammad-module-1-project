from django import template

register = template.Library()


@register.filter
def isDone(arg):
    if arg is True:
        return "Done"
    else:
        return ""
